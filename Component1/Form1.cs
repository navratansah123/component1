﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Component1
{
    /// <summary>
    /// Form 1 class
    /// </summary>
    public partial class Form1 : Form
    {
        //Bitmap to draw which will display on pictureBox.
        const int bitmapX = 640;
        const int bitmapY = 480;
        public Bitmap myBitmap = new Bitmap(bitmapX, bitmapY);
        public Graphics g;

        sheeting mySheeting;

        /// <summary>
        /// Constructor of form 1
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            mySheeting = new sheeting(Graphics.FromImage(myBitmap));
        }

        /// <summary>
        /// Read Instruction provided by the user through Command Prompt, those instruction get converted .
        /// into Lower case and also Triming white spaces.
        /// readCommand to read single Line Instruction.
        /// readMultiCommand to read Multi Line Instruction.
        /// </summary>
        public void ObtainInstruction()
        {
            String readCommand = OutpuBox.Text.Trim().ToLower();
            String readMultiCommand = Output.Text.Trim().ToLower();
            CmdSetup cmdSetup = new CmdSetup();
            cmdSetup.Command(readCommand, readMultiCommand, mySheeting);
            Refresh();
        }

        
        
        private void Runbutton_Click(object sender, EventArgs e)
        {
            ObtainInstruction();
        }

        private void ProgramWindow_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            g.DrawImageUnscaled(myBitmap, 0, 0);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       

        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.ShowDialog();
                myBitmap.Save(saveFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                myBitmap.Load(openFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }

        private void aboutUsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This Application is about Drawing a different shape using command Prompt by user\n"
              + Environment.NewLine + "Programmer Name: Navratan Sah\n"
               + Environment.NewLine + "Thanks For Using This Application"
               , "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        

        private void commandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("All COMMAND Should be Case INSENSITIVE"
            + Environment.NewLine + "======================================\n"
            + Environment.NewLine + "1. moveTo \"To move the pen position\"\nCommand-> moveTo x,y \nWhere x and y is integer value or (X,y) coordinate\n"
            + Environment.NewLine + "2. drawTo \"To Draw Line on Drawing area\"\nCommand-> drawTo x,y \nWhere x and y is integer value or (X,y) coordinate\n"
            + Environment.NewLine + "3. clear \"To Clear the Drawing area\"\nCommand-> clear\n"
            + Environment.NewLine + "4. reset \"To move pen to initial position at the top left of the Screen\"\nCommand-> reset\n"
            + Environment.NewLine + "5. run \"Read the program and Executes it\"\nCommand-> run\n"
            + Environment.NewLine + "6. rectangle \"To Draw Rectangle Shape\"\nCommand-> rectangle width,Height\n"
            + Environment.NewLine + "7. Square \"To Draw Square Shape\"\nCommand-> square width\n"
            + Environment.NewLine + "8. Circle \"To Draw Circle Shape\"\nCommand-> circle radius\n"
            + Environment.NewLine + "9. Triangle \"To Draw Triangle Shape\"\nCommand-> triangle hyp,base,adj\n"
            + Environment.NewLine + "10. Pen color \"To Choose color for drawing\"\nCommand-> pen color \n where color indicate different colour name according to user choice\n"
            + Environment.NewLine + "11. Fill on \"To fill the interior of a polygon\"\nCommand-> fill on\n"
            + Environment.NewLine + "10. loop for \"To Draw Shape using loop for\" Command-> loop for value //statement endfor\n"
            + Environment.NewLine + "11. while loop \"To Draw Shape using while loop\" Command-> if expression //statement endif\n"
            + Environment.NewLine + "12. if condition \"To Draw Shape using if condition\" Command-> while expression //statement endwhile\n"
            + Environment.NewLine + "13. Method\"To Draw Shape using method\" Command-> Method methodName(parameter) //statement endmethod\n"
            + Environment.NewLine + "14. if condition \"To Draw Shape using if condition\" Command-> while expression //statement endwhile\n"
            , "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }
    }
}
