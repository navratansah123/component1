﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Component1
{
    /// <summary>
    /// This class hold method and properties for command instruction and set them in proper formate
    /// also error checking is done By this class.
    /// </summary>
    public class CmdSetup
    {
        int line = 0;

        /// <summary>
        /// Command get executed based on single line or multi Line command
        /// </summary>
        /// <param name="readCommand">hold single Line Command</param>
        /// <param name="readMultiCommand">hold multi Line Command</param>
        /// <param name="mySheeting">Hold caanvas to draw on</param>
        public void Command(String readCommand, String readMultiCommand, sheeting mySheeting)
        {
            //Remove previous excuted program if myTerminal get error, provide new execution area 
            if (mySheeting.error)
            {
                mySheeting.Reset();
                mySheeting.error = false;
            }

            if (readMultiCommand.Length.Equals(0))
            {

                SingleCommand(readCommand, mySheeting);
            }
            else if (readCommand.Equals("run"))
            {
                MultiCommand(readMultiCommand, mySheeting);
            }
            else
            {
                MultiCommand(readMultiCommand, mySheeting);
            }
        }

        /// <summary>
        /// This method split command and parameter when white space occurs between them.
        /// </summary>
        /// <param name="readCommand">pass single line command</param>
        /// <param name="mySheeting">Hold caanvas to draw on</param>
        public void SingleCommand(String readCommand, sheeting mySheeting)
        {
            String[] readCmd = readCommand.Split(' ');
            ParameterSeperator(readCmd, mySheeting, -1);
        }

        /// <summary>
        /// This method split new line, store those value/command.
        /// </summary>
        /// <param name="readCommand">Pass Multi line command from ProgramWindow</param>
        /// <param name="mySheeting">Hold caanvas to draw on</param>
        public void MultiCommand(String readCommand, sheeting mySheeting)
        {
            String[] value = readCommand.Split('\n');
            int num = 0;
            int x = 0;
            try
            {
                //This loop run until number of input get exists in program window
                while (num < value.Length)
                {
                    String[] readCmd = value[num].Split(' ');
                    //if user enter value and while get equal then value will be added in list data
                    if (readCmd[0].Equals("while"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endwhile"));
                        CmdLoopWhile(listData, mySheeting, num, x);
                    }

                    else if (readCmd[0].Equals("if"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endif"));
                        CmdConditionIf(listData, mySheeting, num, x);
                    }

                    else if (readCmd[0].Equals("loop"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endfor"));
                        CmdLoopFor(listData, mySheeting, num, x);
                    }

                    else if (readCmd[0].Equals("method"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endmethod"));
                        CmdMethodSelect(listData, mySheeting, num, x);
                    }
                    else
                    {
                        ParameterSeperator(readCmd, mySheeting, num);    //call ParameterSeperator method
                    }
                    num++;
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// This method returns while loop operation when user enter while loop with statement in them.
        /// </summary>
        /// <param name="listData">value provided by user in program window</param>
        /// <param name="mySheeting">Hold caanvas to draw on</param>
        /// <param name="num">line from where command get executed</param>
        /// <param name="z">gives error on line number</param>
        public void CmdLoopWhile(List<String> listData, sheeting mySheeting, int num, int z)
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            int x = 0;
            bool isValueExists = false;
            List<String> stringLists = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringLists.Add(value[allValue]);
                allValue++;
            }

            try
            {
                if (readCmd[1].Split('<').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('<');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mySheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mySheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mySheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mySheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            while (mySheeting.storeVariable.GetVar(tempVal[0]) < x)
                            {
                                newData = string.Join("\n", stringLists);
                                MultiCommand(newData, mySheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split('>').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('>');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mySheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mySheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mySheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mySheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            while (mySheeting.storeVariable.GetVar(tempVal[0]) > x)
                            {
                                newData = string.Join("\n", stringLists);
                                MultiCommand(newData, mySheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split("==".ToCharArray()).Length > 1)
                {
                    String[] tempVal = readCmd[1].Split("==".ToCharArray());
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mySheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mySheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mySheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mySheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            while (mySheeting.storeVariable.GetVar(tempVal[0]) == x)
                            {
                                newData = string.Join("\n", stringLists);
                                MultiCommand(newData, mySheeting);
                            }
                        }
                    }
                }
                else
                {
                    mySheeting.checkSyntax.ParameterCheck(false, "", num - z, mySheeting, line);
                    line = line + 20;
                }
            }
            catch
            {
                mySheeting.checkSyntax.ParameterCheck(false, "", num - z, mySheeting, line);
                line = line + 20;
            }

        }

        /// <summary>
        /// This method execute if condition when user enter if with statement in them.
        /// </summary>
        /// <param name="listData">value provided by user in program window</param>
        /// <param name="mySheeting">Hold caanvas to draw on</param>
        /// <param name="num">line from where command get executed</param>
        /// <param name="z">gives error on line number</param>
        public void CmdConditionIf(List<String> listData, sheeting mySheeting, int num, int z)
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            int x = 0;
            bool isValueExists = false;
            List<String> stringList = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringList.Add(value[allValue]);
                allValue++;
            }
            try
            {
                if (readCmd[1].Split('<').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('<');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mySheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mySheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mySheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mySheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mySheeting.storeVariable.GetVar(tempVal[0]) < x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData, mySheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split('>').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('>');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mySheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mySheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mySheeting.storeVariable.GetVar(tempVal[0]) < x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData, mySheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split('>').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('>');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mySheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mySheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mySheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mySheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mySheeting.storeVariable.GetVar(tempVal[0]) > x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData, mySheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split("==".ToCharArray()).Length > 1)
                {
                    String[] tempVal = readCmd[1].Split("==".ToCharArray());
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mySheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mySheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mySheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mySheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mySheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mySheeting.storeVariable.GetVar(tempVal[0]) > x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData, mySheeting);
                            }
                        }
                    }
                }
                else
                {
                    mySheeting.checkSyntax.ParameterCheck(false, "", num - z, mySheeting, line);
                    line = line + 20;
                }
            }
            catch
            {
                mySheeting.checkSyntax.ParameterCheck(false, "", num - z, mySheeting, line);
                line = line + 20;
            }
        }

        /// <summary>
        /// This method execute for loop operation when user enter for with statement in them.
        /// </summary>
        /// <param name="listData">value provided by user in program window</param>
        /// <param name="mySheeting">Hold caanvas to draw on</param>
        /// <param name="num">line from where command get executed</param>
        /// <param name="z">gives error on line</param>
        public void CmdLoopFor(List<String> listData, sheeting mySheeting, int num, int z)//LoopFor
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            int x = 0;
            bool isValueExists = false;
            List<String> stringLists = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringLists.Add(value[allValue]);
                allValue++;
            }
            try
            {
                if (readCmd[1].Equals("for"))
                {
                    if (!int.TryParse(readCmd[2], out x))
                    {
                        if (!mySheeting.storeVariable.VarExists(readCmd[2]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mySheeting.storeVariable.GetVar(readCmd[2]);
                        }
                    }
                }
                if (isValueExists)
                {
                    mySheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mySheeting, line);
                    line = line + 20;
                }
                else
                {
                    for (int b = 0; b < x; b++)
                    {
                        newData = string.Join("\n", stringLists);
                        MultiCommand(newData, mySheeting);
                    }
                }
            }
            catch
            {
                mySheeting.checkSyntax.ParameterCheck(false, "", num - z, mySheeting, line);
                line = line + 20;
            }
        }


        public void CmdMethodSelect(List<String> listData, sheeting mySheeting, int num, int z)
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            String x = null;
            bool isValueExists = false;
            String[] method = readCmd[1].Split('(', ')');
            String[] methodValue = null;

            mySheeting.storeMethod.StoreVar(method[0], method[1]);
            List<String> stringLists = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringLists.Add(value[allValue]);
                allValue++;
            }
            try
            {
                if (mySheeting.storeMethod.VarExists(method[0]))
                {
                    x = mySheeting.storeMethod.GetVar(method[0]);
                    methodValue = x.Split(',');
                }
                else
                {
                    isValueExists = true;
                }
                if (isValueExists)
                {
                    mySheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mySheeting, line);
                    line = line + 20;
                }
                else
                {
                    newData = string.Join("\n", stringLists);
                    String methodCmd = method[0] + "command";
                    mySheeting.storeMethod.StoreVar(methodCmd, newData);
                }

            }
            catch
            {
                mySheeting.checkSyntax.ParameterCheck(false, "", num - z, mySheeting, line);
                line = line + 20;
            }

        }


        public void ParameterSeperator(String[] readCmd, sheeting mySheeting, int num)
        {
            try
            {
                String[] method = readCmd[0].Split('(', ')');
                if (readCmd[0].Equals("drawto"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    bool isVarExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[0]))
                            {
                                isVarExists = true;
                            }
                            else
                            {
                                x = mySheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isVarExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[0], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[1]))
                            {
                                isVarExists = true;
                            }
                            else
                            {
                                y = mySheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isVarExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[1], num, mySheeting, line);
                                num = num + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        mySheeting.DrawLine(x, y);
                    }
                }

                else if (readCmd[0].Equals("moveto"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[0]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mySheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[0], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                y = mySheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[1], num, mySheeting, line);
                                num = num + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        mySheeting.MoveTo(x, y);
                    }
                }

                else if (readCmd[0].Equals("rectangle"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[0]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mySheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[0], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                y = mySheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[1], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        Shape drawRectangle = new DrawSquare(x, y);
                        drawRectangle.Draw(mySheeting);
                    }
                }
                else if (readCmd[0].Equals("square"))
                {
                    int x = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(readCmd[1], out x))
                        {
                            if (!mySheeting.storeVariable.VarExists(readCmd[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mySheeting.storeVariable.GetVar(readCmd[1]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        Shape drawSquare = new DrawSquare(x, x);
                        drawSquare.Draw(mySheeting);
                    }
                }

                else if (readCmd[0].Equals("triangle"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    int z = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[0]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mySheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[0], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                y = mySheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[1], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[2], out z))
                        {
                            if (!mySheeting.storeVariable.VarExists(data[2]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                z = mySheeting.storeVariable.GetVar(data[2]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, data[2], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        Shape drawTriangle = new DrawTriangle(x, y, z);
                        drawTriangle.Draw(mySheeting);
                    }
                }
                else if (readCmd[0].Equals("circle"))
                {
                    int x = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(readCmd[1], out x))
                        {
                            if (!mySheeting.storeVariable.VarExists(readCmd[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mySheeting.storeVariable.GetVar(readCmd[1]);
                            }
                            if (isValueExists)
                            {
                                mySheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mySheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        Shape drawCircle = new DrawCircle(x);
                        drawCircle.Draw(mySheeting);
                    }
                }
                else if (readCmd[0].Equals("polygon"))
                {
                    String[] data = readCmd[1].Split(',');
                    List<int> listPoints = new List<int>();
                    int i = 0;
                    int x = 0;
                    bool isValueExists = false;
                    try
                    {
                        while (data.Length > i)
                        {
                            if (!int.TryParse(data[i], out x))
                            {
                                if (!mySheeting.storeVariable.VarExists(data[i]))
                                {
                                    isValueExists = true;
                                }
                                else
                                {
                                    listPoints.Add(mySheeting.storeVariable.GetVar(data[i]));
                                }
                                if (isValueExists)
                                {
                                    mySheeting.checkSyntax.ParameterCheck(false, data[i], num, mySheeting, line);
                                    line = line + 20;
                                }
                            }
                            listPoints.Add(x);
                            i++;
                        }
                    }
                    catch (Exception e)
                    {
                        mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        int[] polyArrays = listPoints.ToArray();
                       
                    }
                }
                else if (readCmd[0].Equals("pen"))
                {
                    Color color = Color.FromName(readCmd[1]); //User input color 

                    if (color.IsKnownColor == false)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mySheeting, line);
                        line = line + 20;
                    }
                    if (!mySheeting.error)
                    {
                        mySheeting.Set_Pen_Color(color);
                    }
                }
                else if (readCmd[0].Equals("fill"))
                {
                    bool fillOn = readCmd[1].Equals("on");
                    bool fillOff = readCmd[1].Equals("off");

                    if (fillOn == false && fillOff == false)
                    {
                        mySheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mySheeting, line);
                        line = line + 20;
                    }

                    if (!mySheeting.error)
                    {
                        if (fillOn)
                        {
                            mySheeting.fill = true;
                        }
                        else if (fillOff)
                        {
                            mySheeting.fill = false;
                        }
                    }
                }
                else if (readCmd[0].Equals("clear"))
                {
                    if (!mySheeting.error)
                    {
                        mySheeting.Clear();
                    }
                }
                else if (readCmd[0].Equals("reset"))
                {
                    if (!mySheeting.error)
                    {
                        mySheeting.Reset();
                    }
                }
                else if (readCmd[0].Equals("exit"))
                {
                    if (!mySheeting.error)
                    {
                        Application.Exit();
                    }
                }
                else if (mySheeting.storeMethod.VarExists(method[0]))
                {
                    String[] methodValue = (mySheeting.storeMethod.GetVar(method[0])).Split(',');
                    String methodCmd = method[0] + "command";
                    String methodCommand = mySheeting.storeMethod.GetVar(methodCmd);
                    String[] userValue = method[1].Split(',');
                    int x = 0;
                    while (methodValue.Length > x)
                    {
                        String[] valueStore = (methodValue[x] + " = " + userValue[x]).Split(' ');
                        ParameterSeperator(valueStore, mySheeting, num);
                        x++;
                    }
                    MultiCommand(methodCommand, mySheeting);

                }
                else if (readCmd[1].Equals("="))
                {
                    try
                    {
                        if (readCmd[3].Equals("+"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mySheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mySheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mySheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mySheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                                line = line + 20;
                            }
                            varValue = x + y;
                            mySheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                        if (readCmd[3].Equals("-"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mySheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mySheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mySheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mySheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                                line = line + 20;
                            }
                            varValue = x - y;
                            mySheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                        if (readCmd[3].Equals("/"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mySheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mySheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mySheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mySheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                                line = line + 20;
                            }
                            varValue = x / y;
                            mySheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                        if (readCmd[3].Equals("*"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mySheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mySheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mySheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mySheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mySheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                                line = line + 20;
                            }
                            varValue = x * y;
                            mySheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                    }
                    catch
                    {
                        int x = 0;
                        try
                        {
                            bool isValueExists = false;
                            if (!int.TryParse(readCmd[2], out x))
                            {
                                if (!mySheeting.storeVariable.VarExists(readCmd[2]))
                                {
                                    isValueExists = true;
                                }
                                else
                                {
                                    x = mySheeting.storeVariable.GetVar(readCmd[2]);
                                }
                                if (isValueExists)
                                {
                                    mySheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mySheeting, line);
                                    line = line + 20;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            mySheeting.checkSyntax.ParameterCheck(e, num, mySheeting, line);
                            line = line + 20;
                        }
                        if (!mySheeting.error)
                        {
                            if (!mySheeting.storeVariable.VarExists(readCmd[0]))
                            {
                                mySheeting.storeVariable.StoreVar(readCmd[0], x);
                            }
                            else
                            {
                                mySheeting.storeVariable.EditVar(readCmd[0], x);
                            }
                        }
                    }
                }
                else
                {
                    mySheeting.checkSyntax.CommandCheck(mySheeting, num, line);
                    line = line + 20;
                }
            }
            catch
            {
                mySheeting.checkSyntax.CommandCheck(mySheeting, num, line);
                line = line + 20;
            }

        }
    }
}
