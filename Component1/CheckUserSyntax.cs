﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Component1
{
    /// <summary>
    /// This class check for Invalid command and parameter Entered by User in single command line
    /// and multicommand/program window.
    /// </summary>
    public class CheckUserSyntax
    {

        /// <summary>
        /// This method gives error message when entered command will be Invalid
        /// </summary>
        /// <param name="mySheeting">This indicate a place where Error message will be Displayed</param>
        /// <param name="num">num indicate line number of Command</param>
        /// <param name="x">It indicate the location where error will be displayed on caanvas</param>
        public void CommandCheck(sheeting mySheeting, int num, int x)
        {
            Font errortxtFont = new Font("Arial", 10);
           
            SolidBrush errortxtBrush = new SolidBrush(Color.Black);
            num++;
            if (num != 0)
            {
                if (x == 0)
                {

                    mySheeting.Reset();
                }

                mySheeting.g.DrawString("Command on line " + (num) + " does not exist", errortxtFont, errortxtBrush, 0, 0 + x);
            }
            else
            {

                mySheeting.g.DrawString("Command does not exist", errortxtFont, errortxtBrush, 0, 0);
            }

            mySheeting.error = true;

        }


        /// <summary>
        /// This method gives error message when entered Parameter will be Invalid
        /// </summary>
        /// <param name="parameter">Gets boolean values according to validity of parameter</param>
        /// <param name="data">Gives line number where Error will be found</param>
        /// <param name="num">num indicate line number for Command</param>
        /// <param name="myCaanvas">This indicate a place where Error message will be Displayed</param>
        /// <param name="x">It indicate the location where error will be displayed on caanvas</param>
        public void ParameterCheck(bool parameter, String data, int num, sheeting mySheeting,  int x)
        {
            
            if (!parameter)
            {
                Font errortxtFont = new Font("Arial", 10);
                SolidBrush errortxtBrush = new SolidBrush(Color.Black);
                if (x == 0)
                {
                    mySheeting.Reset();
                }
                if((num + 1) == 0)
                {

                    mySheeting.g.DrawString("Paramater " + data +  " is invalid", errortxtFont, errortxtBrush, 0, 0 + x);
                }
                else
                {

                    mySheeting.g.DrawString("Paramater " + data + " on line " + (num + 1) + " is invalid", errortxtFont, errortxtBrush, 0, 0 + x);
                }

                mySheeting.error = true;
            }
        }

        /// <summary>
        /// This method gives error message when entered Parameter number will be Invalid and
        /// Catch Exception for invalid command
        /// </summary>
        /// <param name="e">Catch Exception for invalid command</param>
        /// <param name="num">num indicate line number of Command</param>
        /// <param name="myCaanvas">This indicate a place where Error message will be Displayed</param>
        /// <param name="x">It indicate the location where error will be displayed on caanvas</param>
        public void ParameterCheck(Exception e, int num, sheeting mySheeting, int x)
        {
            Font errortxtFont = new Font("Arial", 10);
            SolidBrush errortxtBrush = new SolidBrush(Color.Black);

            if (x == 0)
            {

                mySheeting.Reset();
            }
            if ((num + 1) == 0)
            {

                mySheeting.g.DrawString("Wrong number of parameters inputted", errortxtFont, errortxtBrush, 0, 0 + x);
            }
            else
            {

                mySheeting.g.DrawString("Wrong number of parameters inputted on line"+(num+1), errortxtFont, errortxtBrush, 0, 0 + x);
            }

            mySheeting.error = true;           
        }

    }
}
