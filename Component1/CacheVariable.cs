﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Component1
{
    /// <summary>
    /// This class represent to store, retrive, Edit, delete and Check whether value is Exists in 
    /// Dictionary or not, with their specific keys and its stored value in variable.
    /// </summary>
    public class CacheVariable
    {

        Dictionary<string, int> VarDetail = new Dictionary<string, int>();


        /// <summary>
        /// Represent to add specific key and value of given variable to the Dictionary
        /// </summary>
        /// <param name="varName">store String Key of given variable to the Dictionary</param>
        /// <param name="varValue">store int Value of given variable to the Dictionary</param>
        public void StoreVar(String varName, int varValue)
        {
            VarDetail.Add(varName, varValue);
        }

        /// <summary>
        /// Represent to get value from specific key
        /// </summary>
        /// <param name="varName">Get value of Specific key</param>
        /// <returns>Returns true, if Dictionary contains value with their specific Keys, otherwise false</returns>
        public int GetVar(String varName)
        {
            int x;
            VarDetail.TryGetValue(varName, out x);
            return x;
        }

        /// <summary>
        /// Represent to update value if needed
        /// </summary>
        /// <param name="varName">Represent String Key of given variable</param>
        /// <param name="varValue">Update int value of given key to the Dictionary</param>
        public void EditVar(String varName, int varValue)
        {
            VarDetail[varName] = varValue; 
        }

        /// <summary>
        /// boolean to Check for value is Exists or not
        /// </summary>
        /// <param name="varName">value to check</param>
        /// <returns>Returns true, if Dictionary contains value with their specific Keys, otherwise false</returns>
        public bool VarExists(String varName)
        {
            int x;
            return VarDetail.TryGetValue(varName, out x);
        }

        /// <summary>
        /// Reset or Remove all keys and value from Dictionary
        /// </summary>
        public void Reset()
        {
            VarDetail.Clear(); //Remove all keys and value from Dictionary
        }
    }
}
